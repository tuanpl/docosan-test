@extends('layouts.app')
 
@section('content')
    <!-- Create Task Form... -->
    <div class="panel-body">
        <!-- Display Validation Errors -->
        @include('common.errors')
 
        <!-- New Task Form -->
        <form action="/task" method="POST">
            @csrf
 
            <!-- Task Name -->
            <div class="mb-3">
                    <label for="task-name" class="col-sm-1 form-label">Title</label>
                    <input type="text" name="title" id="task-name" class="form-control">
                
                </div>
                <div class="mb-3">
                    <label for="task-name" class="col-sm-1 form-label">Description</label>
                    <input type="text" name="description" id="task-description" class="form-control">
                
                </div>
                <div class="mb-3">
            <!-- Add Task Button -->
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-plus"></i> Add Task
                    </button>
                </div>
        </form>
    </div>
    <!-- Current Tasks -->
    
        <div class="panel panel-default">
 
            <div class="panel-body">
                <table class="table table-striped task-table">
 
                    <!-- Table Headings -->
                    <thead>
                        <th>Current Tasks 
                            <form action="/tasks" method="POST">
                                @csrf
                                <input type="text" name="searchText" id="task-search-text" value="{{ $searchText ?? '' }}" class="form-control" placeholder="Type to search + Enter">
                            </form> 
                        </th>
                        
                    </thead>
                    @if (count($tasks) > 0)
                    <!-- Table Body -->
                    <tbody>
                        @foreach ($tasks ?? '' as $task)
                            <tr class="d-flex flex-wrap">
                                <!-- Task Name -->
                                <td class="table-title col-3">
                                    <div class="text-break">{{ $task->title }}</div>
                                </td>
                                <td class="table-description col-3">
                                    <div class="text-break">{{ $task->description }}</div>
                                </td>
 
                                <!-- Delete Button -->
                                <td class= "col-2">
                                    <form action="/task/{{ $task->id }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                            
                                        <button class = "btn btn-danger">Delete Task</button>
                                    </form>
                                </td>
                                <td class="col-4">
                                    <li class="list-group-item">
                                        <button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-{{ $loop->index }}" aria-expanded="false">
                                            Edit
                                        </button>

                                        <div class="collapse mt-2" id="collapse-{{ $loop->index }}">
                                        <div class="card card-body">
                                            <form action="{{ url('task/'.$task->id) }}" method="POST">
                                            @csrf
                                            @method('PUT')
                                                <input type="text" name="title" value="{{ $task->title }}">
                                                <input type="text" name="description" value="{{ $task->description }}">
                                                <button class="btn btn-secondary" type="submit">Update</button>
                                            </form>
                                        </div>
                                        </div>
                                    </li>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    @else
                    <tbody>
                    <tr class="col-12"><td>No task found</td></tr>
                    </tbody>
                    @endif
                </table>
            </div>
        </div>
    
@endsection