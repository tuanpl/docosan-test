@extends('layouts.app')
 
@section('content')
    <!-- Create Task Form... -->
    
    <!-- Current Tasks -->
    @if (count($appointments) > 0)
        <div class="panel panel-default">
            <div class="panel-heading">
                Current Tasks
            </div>
 
            <div class="panel-body">
                <table class="table table-striped task-table">
 
                    <!-- Table Headings -->
                    <thead>
                        <th>Appointment</th>
                        <th>&nbsp;</th>
                    </thead>
 
                    <!-- Table Body -->
                    <tbody>
                        @foreach ($appointments ?? '' as $appointment)
                            <tr>
                                <!-- Task Name -->
                                <td class="table-patientid">
                                    <div>{{ $appointment->patient_id }}</div>
                                </td>
                                <td class="table-id">
                                    <div>{{ $appointment->id }}</div>
                                </td>
                                <td class="table-patient-birthday">
                                    <div>{{ $appointment->patient->birthday ?? 'Not available'}}</div>
                                </td>
 
                            </tr>
                        @endforeach
                        {{ $appointments->links() }}
                    </tbody>
                </table>
            </div>
        </div>
    @endif
@endsection