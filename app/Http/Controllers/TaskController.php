<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Repositories\TaskRepository;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TaskController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('XSS');
    }

    public function index(Request $request)
    {
        return view('tasks.index', [ "tasks" =>
            Task::where('user_id', '=', $request->user()->id)
                    ->get()
        ]);
    }
    public function search(Request $request)
    {
        return view('tasks.index', [ 
            "tasks" =>
            Task::select('*')->when($request->has('searchText'), function ($query) use ($request) {
                $query->where('title', 'like','%' .$request->searchText .'%');
            })->where('user_id', '=', $request->user()->id)
                    ->get(),
            "searchText" => $request->searchText
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'description' => 'required|max:255'
        ]);
        $request->user()->tasks()->create([
            'title' => $request->title,
            'description' => $request->description
        ]);

        return redirect('/tasks');

    }

    public function update(Request $request, Task $task)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'description' => 'required|max:255'
        ]);
        $task->update([
            'title' => $request->title,
            'description' => $request->description
        ]);
        return redirect('/tasks');
    }

    public function destroy(Request $request, Task $task)
    {
        $this->authorize('destroy', $task);
        $task->delete();

        return redirect('/tasks');
        //
    }
}