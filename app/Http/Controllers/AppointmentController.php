<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use Illuminate\Http\Request;

class AppointmentController extends Controller
{
    public function index(Request $request)
    {
        return view('appointments.index', [ "appointments" =>
            Appointment::with('Patient')->paginate(10, ['*'], 'page', 2900)
        ]);
    }
}
