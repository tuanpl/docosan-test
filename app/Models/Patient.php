<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $table = 'patients2';
    use HasFactory;
    public function appoinments()
    {
        return $this->hasMany(Appointment::class);
    }
}
