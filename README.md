## Requisitions:
PHP: v7.4
Laravel: v8.x
Node: v18.x
Docker(Optional): v4.x

## Running With Docker
Create new .env file from the existing .env.example, can just copy the file and rename.

Download and install Docker for desktop at https://www.docker.com/products/docker-desktop/

Navigate to root folder of the project and run the following commands to build and start docker container for app:

``` 
docker-compose build app
docker-compose up -d
```

After the the container has start, for first time running, run the following command

```
docker-compose exec app composer install
docker-compose exec app php artisan key:generate
docker-compose run npm npm install  
docker-compose run npm npm run prod
```

Navigate to http://127.0.0.1:8000/ to start using the app.

To stop the container:
```
docker-compose down
```

To stop the container and clear the volume:
```
docker-compose down -v
```

## Running locally:
Create new .env file from the existing .env.example, can just copy the file and rename.

Install PHP and composer
https://www.php.net/manual/en/install.php
https://getcomposer.org/download/

Install node, npm:
https://docs.npmjs.com/downloading-and-installing-node-js-and-npm

Connect to MySQL server of choice.

Create empty DB docosan-test

Run the attached script file docker-compose\mysql\init_db.sql

edit the ".env" file for current DB connection detail:
```
DB_CONNECTION=mysql
DB_HOST=db
DB_PORT=3306
DB_DATABASE=docosantest
DB_USERNAME=test
DB_PASSWORD=test1@docosan
```

Run the following command
```
composer install
php artisan key:generate
npm install
npm run prod
```

To start the service locall: 
```
php artisan serve
```
