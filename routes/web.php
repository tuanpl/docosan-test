<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::group([
'namespace' => 'App\Http\Controllers',
], function ($router) {
// Authentication Routes...
Route::get('/login', 'AuthController@showLoginForm');
Route::post('/login', 'AuthController@login');
Route::get('/logout', 'AuthController@logout');
 
// Registration Routes...
Route::get('/register', 'AuthController@showRegistrationForm');

Route::post('/register', 'AuthController@register');
Route::get('/tasks', 'TaskController@index');
Route::post('/task', 'TaskController@store');
Route::post('/tasks', 'TaskController@search');
Route::delete('/task/{task}', 'TaskController@destroy');
Route::put('/task/{task}', 'TaskController@update');

Route::get('/appointments', 'AppointmentController@index');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
